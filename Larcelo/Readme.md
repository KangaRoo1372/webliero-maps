## Larcelo's maps

Some custom maps created by Larcelo or found among his Liero 1.33 / 1.36 TCs.

Maps WOJNA (1-8) were created by Larcelo for his own Liero 1.36 TC called "Wojenny Mod" (War Mod).

Maps Ando_Prime, Ando_Prime_2, Aqualiris, Malastare, Malastare_2, Moon_Gaza, Moon_Gaza_2, Oxo, Oxo_2, Tatooine and Tatooine2 were created by Larcelo for his unfinished Liero 1.33 racing TC.

Maps FOOD1, FOOD2 and KFC were created by Larcelo for his own Liero 1.33 TC called Food Fight.

Another maps created by Larcelo (a.k.a. Rozpierd): Bunkier, W Zamku, W Piachu, Pvp Arena, Robaleqq, Kamien, Mars1, Tornee1 and Duel (1-5).
