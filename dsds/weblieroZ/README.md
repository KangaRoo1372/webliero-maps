## WeblieroZ maps

maps made for Kangur's DragonballZ inspired mod "WeblieroZ"

most of those maps except *pilaf* & *pilaf_small* use specific custom palettes

*lookout_ext_fish*, *lookout_ext_skull* look like perfect copies of *lookout_ext* but they are actually different (they include shoot through material patterns hidden in the dirt)

