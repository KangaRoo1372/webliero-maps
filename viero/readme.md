## Lord_of_the_Hamsters (LOTH) & Bor'Sucks (BSX) maps for Liero Vietnam TC

Custom maps created by Lord_of_the_Hamsters (LOTH) & Bor'Sucks (BSX) for their own Liero 1.33 TC called "Liero Vietnam" (a.k.a. viero).

Map "Ruins3" is a remake of original viero map "Ruins", made by Larcelo (probably).
