# CHAOS Pool maps

chaos pool are procedurally generated maps, some use existing maps as base, or existing images..  
others use fully procedural generation  
these maps are also available here: https://liero.phazon.xyz/pool/chaos/  

other generated maps by wgetch, intended for "cs reworked", are available there https://gitlab.com/sylvodsds/csmodgen
